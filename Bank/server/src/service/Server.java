package service;

import dao.AgenceDao;
import dao.ClientDao;
import dao.CompteDao;
import dao.OperationDao;
import entities.Agence;
import entities.Client;
import entities.Compte;
import entities.Operation;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

/**
 *
 * @author babacarbasse
 */
public class Server {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        new Server();
    }
    
    public Server() {
        try {
            ServerSocket server = new ServerSocket(9999);
            System.out.println("Server start: "+9999);
            int numClient = 1;
            while(true) {
                Socket socket = server.accept();
                InetAddress adr = socket.getInetAddress();
                String ipclient = adr.getHostAddress();
                String nomclient= adr.getHostName();
                System.out.println("client n°: "+numClient+" adresse ip: "+ipclient+"\n");
                System.out.println("nom machine cliente: "+nomclient+"\n");
                Service s = new Service(socket);
                s.start();
                numClient++;
            }
        } catch(IOException ex) {
            System.err.println("Error start server: "+ ex.toString());
        }
    }
    
    class Service extends Thread {
        private Socket socket;
        private AgenceDao agenceDao;
        private ClientDao clientDao;
        private CompteDao compteDao;
        private OperationDao operationDao;

        public Service(Socket socket) {
            this.socket = socket;
            this.agenceDao = new AgenceDao();
            this.clientDao = new ClientDao();
            this.compteDao = new CompteDao();
            this.operationDao = new OperationDao();
        }
        
        @Override
        public void run() {
            try {
                ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
                ObjectOutputStream oos=new ObjectOutputStream(socket.getOutputStream());
                // DataOutputStream out = new DataOutputStream(socket.getOutputStream());
                // DataInputStream in = new DataInputStream(socket.getInputStream());

                String request;
                do {
                    request = (String)ois.readObject();
                    System.out.println("Une nouvelle requete en cours de traitement !!!");
                    switch(request) {
                        case "add_agence" :
                            Agence agence = (Agence) ois.readObject();
                            agenceDao.ajoutAgence(agence);
                            break;
                        case "liste_agence" :
                            oos.writeObject(agenceDao.listeAgence());
                            oos.flush();
                            break;
                        case "get_numAg_by_nom":
                            String nomAgence = (String) ois.readObject();
                            System.err.println(nomAgence);
                            int numAg = agenceDao.getNumAgByName(nomAgence);
                            System.err.println(numAg);
                            oos.writeObject(numAg);
                            oos.flush();
                            break;
                        case "add_client":
                            Client client = (Client) ois.readObject();
                            clientDao.ajoutClient(client);
                            break;
                        case "liste_client_agence":
                            String nomAg = (String) ois.readObject();
                            oos.writeObject(clientDao.listeClientParAgence(nomAg));
                            oos.flush();
                            break;
                        case "add_compte" :
                            Compte compte = (Compte) ois.readObject();
                            this.compteDao.ajoutCompte(compte);
                            break;
                        case "liste_compte_client":
                            int numCli = (Integer) ois.readObject();
                            oos.writeObject(compteDao.listCompteClient(numCli));
                            oos.flush();
                            break;
                        case "add_operation_compte":
                            Operation operation = (Operation) ois.readObject();
                            operationDao.ajoutOperation(operation);
                            break;
                        case "liste_operation_compte_client":
                            String numCompte = (String) ois.readObject();
                            oos.writeObject(operationDao.listeOperationCptClient(numCompte));
                            oos.flush();
                            break;
                        default:
                            break;
                    }
                } while(true);
            } catch(IOException | ClassNotFoundException ex) {
                System.out.println("****" + ex.getMessage());
            }
        }
    }
}
