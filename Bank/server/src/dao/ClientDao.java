/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import entities.Client;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author babacarbasse
 */
public class ClientDao {
    private AccessBD accessDB;
    private PreparedStatement st = null;
    private ResultSet rs = null;

    public ClientDao() {
        this.accessDB = new AccessBD();
    }
    
    public Boolean ajoutClient(Client client) {
        try {
           String sql = "INSERT INTO Client (prenom, nom, numAg) value (?,?,?)";
           st = this.accessDB.getConn().prepareStatement(sql);
           st.setString(1, client.getPrenom());
           st.setString(2, client.getNom());
           st.setInt(3, client.getNumAg());
           st.executeUpdate();
           return true;
        } catch(SQLException e) {
            System.err.println("ERROR add client : " +e.toString());
            return false;
        }
    }
    
    public ArrayList<Client> listeClientParAgence(String nomAgence) {
        ArrayList<Client> liste = new ArrayList<>();
        try {
            int numAgence = new AgenceDao().getNumAgByName(nomAgence);
            String sql = "SELECT * from Client WHERE numAg = ?";
            st = this.accessDB.getConn().prepareStatement(sql);
            st.setInt(1, numAgence);
            rs = st.executeQuery();
            while(rs.next()) {
                liste.add(
                   new Client(rs.getInt("numCli"), rs.getString("prenom"), rs.getString("nom"), numAgence));
            }
        } catch(SQLException e) {
            System.err.println("ERROR list client : " +e.toString());
        }
        
        return liste;
    }
}
