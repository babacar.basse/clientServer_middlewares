/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import entities.Client;
import entities.Compte;
import entities.Operation;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author babacarbasse
 */
public class OperationDao {
    private AccessBD accessDB;
    private PreparedStatement st = null;
    private ResultSet rs = null;
    private CompteDao compteDao;

    public OperationDao() {
        this.accessDB = new AccessBD();
        this.compteDao = new CompteDao();
    }
    public Boolean ajoutOperation(Operation operation) {
        try {
            Compte compte = this.compteDao.getCompte(operation.getNumCpt());
            if (compte == null) {
                return false;
            }
            String sql = "INSERT INTO Operation (libOp, sensOp, montOp, numCpt) values (?,?,?,?)";
            st = this.accessDB.getConn().prepareStatement(sql);
            st.setString(1, operation.getLibOp());
            st.setString(2, operation.getSensOp());
            st.setDouble(3, operation.getMontOp());
            st.setString(4, operation.getNumCpt());
            st.executeUpdate();
            Double newSolde;
            if (compte.getSens().equals("DB")) {
                compte.setSolde(-compte.getSolde());
            }
            if (operation.getSensOp().equals("CR")) {
                newSolde = compte.getSolde() + operation.getMontOp();
            } else {
                newSolde = compte.getSolde() - operation.getMontOp();
            }
            String newSensCpt;
            if (newSolde < 0) {
                newSensCpt = "DB";
                newSolde = -newSolde;
            } else {
                newSensCpt = "CR";
            }
            String sqlUpdateCompte = "UPDATE Compte SET solde = ?, sens = ? where numCpt = ?";
            st = this.accessDB.getConn().prepareStatement(sqlUpdateCompte);
            st.setDouble(1, newSolde);
            st.setString(2, newSensCpt);
            st.setString(3, compte.getNumCpt());
            st.executeUpdate();
            return true;
        } catch (SQLException ex) {
            System.err.println("Error add Operation: "+ ex.toString());
            return false;
        }
    }

    public ArrayList<Operation> listeOperationCptClient(String numCompte) {
        ArrayList<Operation> liste = new ArrayList<>();
        try {
            String sql = "SELECT * from Operation WHERE numCpt = ?";
            st = this.accessDB.getConn().prepareStatement(sql);
            st.setString(1, numCompte);
            rs = st.executeQuery();
            while(rs.next()) {
                liste.add(
                   new Operation(rs.getInt("numOp"), rs.getString("libOp"), rs.getDate("dateOp"), 
                     rs.getTime("dateOp") ,rs.getString("sensOp"), rs.getDouble("montOp"), numCompte)
                );
            }
        } catch(SQLException e) {
            System.err.println("ERROR list client : " +e.toString());
        }
        return liste;
    }
}
