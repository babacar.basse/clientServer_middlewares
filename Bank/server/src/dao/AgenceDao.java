/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import entities.Agence;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author babacarbasse
 */
public class AgenceDao {
    
    private AccessBD accessDB;
    private PreparedStatement st = null;
    private ResultSet rs = null;

    public AgenceDao() {
        this.accessDB = new AccessBD();
    }
    
    public Boolean ajoutAgence(Agence a) {
        try {
            String sql = "INSERT INTO Agence(nomAg, adresseAg) values (?,?)";
            st = this.accessDB.getConn().prepareStatement(sql);
            st.setString(1, a.getNomAg());
            st.setString(2, a.getAdresseAg());
            st.executeUpdate();
            return true;
        } catch(SQLException e) {
            System.out.println("!!! " + e.toString());
            return false;
        }
    }
    
    public ArrayList<Agence> listeAgence() {
        ArrayList<Agence> liste = new ArrayList<>();
        try {
            st = this.accessDB.getConn().prepareCall("SELECT * FROM  Agence");
            rs = st.executeQuery();
            while(rs.next()) {
                liste.add(
                    new Agence(rs.getInt("numAg"), rs.getString("nomAg"), rs.getString("adresseAg"))
                );
            }
        } catch(SQLException e) {
            System.out.println("!!! " + e.toString());   
        }
        return liste;
    }
    
    /**
     * 
     * @param nomAg
     * @return int 
     * return -1 if agence is not found
     */
    public int getNumAgByName(String nomAg) {
        try {
            String sql = "SELECT * from Agence WHERE nomAg=?";
            st = this.accessDB.getConn().prepareStatement(sql);
            st.setString(1, nomAg);
            rs = st.executeQuery();
            if (rs.next()) {
               return rs.getInt("numAg");
            }
            return -1;
        } catch(SQLException e) {
            System.out.println("!!! Error " + e.toString());   
            return -1;
        }
    }
    
    
    
}
