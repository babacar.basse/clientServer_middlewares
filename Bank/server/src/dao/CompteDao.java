/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import entities.Compte;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author babacarbasse
 */
public class CompteDao {
    private AccessBD accessDB;
    private PreparedStatement st = null;
    private ResultSet rs = null;

    public CompteDao() {
        this.accessDB = new AccessBD();
    }
    
    public Boolean ajoutCompte(Compte compte) {
        try {
            String sql = "INSERT INTO Compte (numCpt, libelleCpt, sens, solde, numCli) values (?,?,?,?,?)";
            st = this.accessDB.getConn().prepareStatement(sql);
            st.setString(1, compte.getNumCpt());
            st.setString(2, compte.getLibelleCpt());
            st.setString(3, compte.getSens());
            st.setDouble(4, compte.getSolde());
            st.setInt(5, compte.getNumCli());
            st.executeUpdate();
            return true;
        } catch (SQLException ex) {
            System.err.println("Error add Compte: "+ ex.toString());
            return false;
        }
    }
    
    public ArrayList<Compte> listCompteClient(int numCli) {
        ArrayList<Compte> list = new ArrayList<>();
        try {
            String sql = "SELECT * FROM Compte WHERE numCli = ?";
            st = this.accessDB.getConn().prepareStatement(sql);
            st.setInt(1, numCli);
            rs = st.executeQuery();
            while(rs.next()) {
                list.add(
                  new Compte(rs.getString("numCpt"), rs.getString("libelleCpt"), rs.getString("sens"),
                    rs.getDouble("solde"))
                );
            }
        } catch (SQLException ex) {
            System.err.println("Error list Compte client: "+ ex.toString());
        }
        return list;
    }

    Compte getCompte(String numCpt) {
        Compte compte = null;
        try {
            String sql = "SELECT * FROM Compte WHERE numCpt = ?";
            st = this.accessDB.getConn().prepareStatement(sql);
            st.setString(1, numCpt);
            rs = st.executeQuery();
            if (rs.next()) {
               compte = new Compte(rs.getString("numCpt"), rs.getString("libelleCpt"), rs.getString("sens"),
                    rs.getDouble("solde"));
            }
        } catch (SQLException ex) {
            System.err.println("Error list Compte client: "+ ex.toString());
        }
        return compte;
    }
}
