package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author babacarbasse
 */
public class AccessBD {
    
    private Connection conn = null;
    
    public AccessBD() {
        String login = "root";
        String password = "mamydiagne";
        try {
            Class.forName("com.mysql.jdbc.Driver");
            this.conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/bank", login, password);
            System.out.println("Connexion success !!!");
        } catch(ClassNotFoundException | SQLException e) {
            System.out.println("!!!ERROR " + e.toString());
        }
    }

    /**
     * @return the conn
     */
    public Connection getConn() {
        return conn;
    }

    
    
}
