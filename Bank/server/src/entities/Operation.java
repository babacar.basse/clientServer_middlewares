/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.sql.Time;
import java.util.Date;

/**
 *
 * @author babacarbasse
 */
public class Operation implements Serializable{
    private int numOp;
    private String libOp;
    private String sensOp;
    private Double montOp;
    private String numCpt;
    private Date dateOp;
    private Time timeOp; 
    
    public Operation(String libOp, String sensOP, Double montOp, String numCpt) {
        this.setLibOp(libOp);
        this.setMontOp(montOp);
        this.setSensOp(sensOP);
        this.setNumCpt(numCpt);
    }

    public Operation(int numOp, String libOp, Date dateOp, Time timeOp, String sensOp, double montOp, String numCpt) {
        this(libOp, sensOp, montOp, numCpt);
        this.numOp = numOp;
        this.dateOp = dateOp;
        this.timeOp = timeOp;
    }

    /**
     * @return the numOp
     */
    public int getNumOp() {
        return numOp;
    }

    /**
     * @param numOp the numOp to set
     */
    public void setNumOp(int numOp) {
        this.numOp = numOp;
    }

    /**
     * @return the libOp
     */
    public String getLibOp() {
        return libOp;
    }

    /**
     * @param libOp the libOp to set
     */
    public void setLibOp(String libOp) {
        this.libOp = libOp;
    }

    /**
     * @return the dateOp
     */
    public Date getDateOp() {
        return dateOp;
    }

    /**
     * @param dateOp the dateOp to set
     */
    public void setDateOp(Date dateOp) {
        this.dateOp = dateOp;
    }

    /**
     * @return the sensOp
     */
    public String getSensOp() {
        return sensOp;
    }

    /**
     * @param sensOp the sensOp to set
     */
    public void setSensOp(String sensOp) {
        this.sensOp = sensOp;
    }

    /**
     * @return the montOp
     */
    public Double getMontOp() {
        return montOp;
    }

    /**
     * @param montOp the montOp to set
     */
    public void setMontOp(Double montOp) {
        this.montOp = montOp;
    }

    /**
     * @return the numCpt
     */
    public String getNumCpt() {
        return numCpt;
    }

    /**
     * @param numCpt the numCpt to set
     */
    public void setNumCpt(String numCpt) {
        this.numCpt = numCpt;
    }

    /**
     * @return the timeOp
     */
    public Time getTimeOp() {
        return timeOp;
    }

    /**
     * @param timeOp the timeOp to set
     */
    public void setTimeOp(Time timeOp) {
        this.timeOp = timeOp;
    }
}
