/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;

/**
 *
 * @author babacarbasse
 */
public class Compte implements Serializable{
    private String numCpt;
    private String libelleCpt;
    private String sens;
    private Double solde;
    private int numCli;
    
    public Compte(String libelleCpt, int numCli) {
        this.setNumCli(numCli);
        this.setLibelleCpt(libelleCpt);
        this.sens = "CR";
        this.solde = 0.0;
        this.numCpt =  this.generate(10)+"-"+numCli;
    }

    public Compte(String numCpt, String libelleCpt, String sens, double solde) {
        this.numCpt = numCpt;
        this.libelleCpt = libelleCpt;
        this.sens = sens;
        this.solde = solde;
    }

    /**
     * @return the numCpt
     */
    public String getNumCpt() {
        return numCpt;
    }

    /**
     * @param numCpt the numCpt to set
     */
    public void setNumCpt(String numCpt) {
        this.numCpt = numCpt;
    }

    /**
     * @return the libelleCpt
     */
    public String getLibelleCpt() {
        return libelleCpt;
    }

    /**
     * @param libelleCpt the libelleCpt to set
     */
    public void setLibelleCpt(String libelleCpt) {
        this.libelleCpt = libelleCpt;
    }

    /**
     * @return the sens
     */
    public String getSens() {
        return sens;
    }

    /**
     * @param sens the sens to set
     */
    public void setSens(String sens) {
        this.sens = sens;
    }

    /**
     * @return the solde
     */
    public Double getSolde() {
        return solde;
    }

    /**
     * @param solde the solde to set
     */
    public void setSolde(Double solde) {
        this.solde = solde;
    }

    /**
     * @return the numCli
     */
    public int getNumCli() {
        return numCli;
    }

    /**
     * @param numCli the numCli to set
     */
    public void setNumCli(int numCli) {
        this.numCli = numCli;
    }

    
    public String generate(int length) {
        String chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        String pass = "";
        for(int x=0;x<length;x++)
        {
           int i = (int)Math.floor(Math.random() * 36);
           pass += chars.charAt(i);
        }
        return pass;
    }
}
