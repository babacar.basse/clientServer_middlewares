/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import entities.Agence;
import entities.Client;
import entities.Compte;
import entities.Operation;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;

/**
 *
 * @author babacarbasse
 */
public class ClientService {
    private Socket socket;
    private DataOutputStream out;
    private DataInputStream in;
    private ObjectOutputStream oos;
    private ObjectInputStream ois;
    
    public ClientService() {
        try {
            socket = new Socket("localhost", 9999);
            out = new DataOutputStream(getSocket().getOutputStream());
            in = new DataInputStream(getSocket().getInputStream());
            oos = new ObjectOutputStream(getSocket().getOutputStream());
            ois = new ObjectInputStream(getSocket().getInputStream());
            System.out.println("success !!!");
        } catch(IOException ex) {
            System.out.println(ex.getMessage());
        }
    }

    /**
     * @return the socket
     */
    public Socket getSocket() {
        return socket;
    }

    /**
     * @param socket the socket to set
     */
    public void setSocket(Socket socket) {
        this.socket = socket;
    }

    /**
     * @return the out
     */
    public DataOutputStream getOut() {
        return out;
    }

    /**
     * @param out the out to set
     */
    public void setOut(DataOutputStream out) {
        this.out = out;
    }

    /**
     * @return the in
     */
    public DataInputStream getIn() {
        return in;
    }

    /**
     * @param in the in to set
     */
    public void setIn(DataInputStream in) {
        this.in = in;
    }

    /**
     * @return the oos
     */
    public ObjectOutputStream getOos() {
        return oos;
    }

    /**
     * @param oos the oos to set
     */
    public void setOos(ObjectOutputStream oos) {
        this.oos = oos;
    }

    /**
     * @return the ois
     */
    public ObjectInputStream getOis() {
        return ois;
    }

    /**
     * @param ois the ois to set
     */
    public void setOis(ObjectInputStream ois) {
        this.ois = ois;
    }
    
    public ArrayList<Agence> getListeAgence() {
        ArrayList<Agence> listeAgence = new ArrayList<>();
        try {
            this.getOos().writeObject("liste_agence");
            this.getOos().flush();
            listeAgence = (ArrayList<Agence>) this.getOis().readObject();
        } catch (IOException | ClassNotFoundException ex) {
            System.err.println("Error List Agence: " + ex.toString());
        }
        return listeAgence;
    }

    public void addAgence(Agence agence) {
        try {
            this.getOos().writeObject("add_agence");
            this.getOos().writeObject(agence);
            this.getOos().flush();   
        } catch(IOException e) {
            System.err.println("Error Add Agence: " + e.toString());
        }
    }

    public int getNumAgenceByNom(String agence) {
        int numAgence = -1;
        try {
            this.getOos().writeObject("get_numAg_by_nom");
            this.getOos().writeObject(agence);
            this.getOos().flush();   
            numAgence = (int) this.getOis().readObject();
        } catch (IOException | ClassNotFoundException ex) {
            System.err.println("Error List Agence: " + ex.toString());
        }
        return numAgence;
    }
    
    public void addClient(Client client) {
        try {
            this.getOos().writeObject("add_client");
            this.getOos().writeObject(client);
            this.getOos().flush();
        } catch(IOException e) {
            System.err.println("Error Add Client: " + e.toString());
        }
    }
    
    public ArrayList<Client> getListeClientAgence(String nomAgence) {
        ArrayList<Client> liste = new ArrayList<>();
        try {
            this.getOos().writeObject("liste_client_agence");
            this.getOos().writeObject(nomAgence);
            this.getOos().flush();
            liste = (ArrayList<Client>) this.getOis().readObject();
        } catch (IOException | ClassNotFoundException ex) {
            System.err.println("Error List Client Agence: " + ex.toString());
        }
        return liste;
    }
    
    public void addCompte(Compte compte) {
        try {
            this.getOos().writeObject("add_compte");
            this.getOos().writeObject(compte);
            this.getOos().flush();   
        } catch(IOException e) {
            System.err.println("Error Add Agence: " + e.toString());
        }
    }
    
    public ArrayList<Compte> getListeCompteClient(int numCli) {
        ArrayList<Compte> liste = new ArrayList<>();
        try {
            this.getOos().writeObject("liste_compte_client");
            this.getOos().writeObject(numCli);
            this.getOos().flush();
            liste = (ArrayList<Compte>) this.getOis().readObject();
        } catch (IOException | ClassNotFoundException ex) {
            System.err.println("Error List Compte client: " + ex.toString());
        }
        return liste;
    }
    
    public void addOperationCompte(Operation operation) {
        try {
            this.getOos().writeObject("add_operation_compte");
            this.getOos().writeObject(operation);
            this.getOos().flush();   
        } catch(IOException e) {
            System.err.println("Error Add Agence: " + e.toString());
        }
    }

    public ArrayList<Operation> getListeOperationCptClient(String numCompte) {
        ArrayList<Operation> liste = new ArrayList<>();
        try {
            this.getOos().writeObject("liste_operation_compte_client");
            this.getOos().writeObject(numCompte);
            this.getOos().flush();
            liste = (ArrayList<Operation>) this.getOis().readObject();
        } catch (IOException | ClassNotFoundException ex) {
            System.err.println("Error List Operation compte client: " + ex.toString());
        }
        return liste;
    }
}
