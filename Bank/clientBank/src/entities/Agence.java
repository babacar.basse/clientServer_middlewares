/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;

/**
 *
 * @author babacarbasse
 */
public class Agence implements Serializable {
    private int numAg;
    private String nomAg;
    private String adresseAg;
    
    public Agence(String nomAg, String adresseAg) {
        this.setNomAg(nomAg);
        this.setAdresseAg(adresseAg);
    }

    public Agence(int numAg, String nomAg, String adresseAg) {
        this(nomAg, adresseAg);
        this.numAg = numAg;
    }

    /**
     * @return the numAg
     */
    public int getNumAg() {
        return numAg;
    }

    /**
     * @param numAg the numAg to set
     */
    public void setNumAg(int numAg) {
        this.numAg = numAg;
    }

    /**
     * @return the nomAg
     */
    public String getNomAg() {
        return nomAg;
    }

    /**
     * @param nomAg the nomAg to set
     */
    public void setNomAg(String nomAg) {
        this.nomAg = nomAg;
    }

    /**
     * @return the adresseAg
     */
    public String getAdresseAg() {
        return adresseAg;
    }

    /**
     * @param adresseAg the adresseAg to set
     */
    public void setAdresseAg(String adresseAg) {
        this.adresseAg = adresseAg;
    }

}
