/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;

/**
 *
 * @author babacarbasse
 */
public class Client implements Serializable{
    private int numCli;
    private String prenom;
    private String nom;
    private int numAg;
    
    public Client(String nom, String prenom, int numAg) {
        this.setNom(nom);
        this.setPrenom(prenom);
        this.setNumAg(numAg);
    }

    public Client(int numCli, String prenom, String nom, int numAgence) {
        this(nom, prenom, numAgence);
        this.numCli = numCli;
    }

    /**
     * @return the numCli
     */
    public int getNumCli() {
        return numCli;
    }

    /**
     * @param numCli the numCli to set
     */
    public void setNumCli(int numCli) {
        this.numCli = numCli;
    }

    /**
     * @return the prenom
     */
    public String getPrenom() {
        return prenom;
    }

    /**
     * @param prenom the prenom to set
     */
    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    /**
     * @return the nom
     */
    public String getNom() {
        return nom;
    }

    /**
     * @param nom the nom to set
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * @return the numAg
     */
    public int getNumAg() {
        return numAg;
    }

    /**
     * @param numAg the numAg to set
     */
    public void setNumAg(int numAg) {
        this.numAg = numAg;
    }

}
